namespace Publisher.Controllers;

[ApiController]
[Route("[controller]")]
public class ProducerController : ControllerBase
{
    private readonly IBus _bus;
    private readonly ILogger<ProducerController> _logger;

    public ProducerController(ILogger<ProducerController> logger, IBus bus)
    {
        _logger = logger;
        _bus = bus;
    }

    [HttpPost(Name = "CreateMessage")]
    public async Task<IActionResult> Post(string message)
    {
        await _bus.Publish(new SimpleMessage(message));
        return Ok();
    }
}