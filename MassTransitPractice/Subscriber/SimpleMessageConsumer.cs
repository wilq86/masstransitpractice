﻿namespace Publisher;

public class SimpleMessageConsumer : IConsumer<SimpleMessage>
{
    readonly ILogger<SimpleMessageConsumer> _logger;

    public SimpleMessageConsumer(ILogger<SimpleMessageConsumer> logger)
    {
        _logger = logger;
    }

    public Task Consume(ConsumeContext<SimpleMessage> context)
    {
        _logger.LogInformation("Received Text: {Text}", context.Message.Message);
        return Task.CompletedTask;
    }
}
